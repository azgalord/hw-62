import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Home from "../../components/Home/Home";
import AboutUs from "../../components/AboutUs/AboutUs";
import Contacts from "../../components/Contacts/Contacts";
import Star1 from '../../assets/img/star1.jpg';
import Star2 from '../../assets/img/star2.jpg';
import Star3 from '../../assets/img/star3.jpg';

import './App.css';

class App extends Component {
  state = {
    filmCards: [
      {
        background: Star1,
        title: 'Star Wars: Episode I - The Phantom Menace',
        text: 'Star Wars: Episode I – The Phantom Menace is a 1999 American epic space opera written and directed by George Lucas, produced by Lucasfilm and distributed by 20th Century Fox. It is the first installment in the Star Wars prequel trilogy and stars Liam Neeson, Ewan McGregor, Natalie Portman, Jake Lloyd, Ian McDiarmid, Anthony Daniels, Kenny Baker, Pernilla August, and Frank Oz.'
      },
      {
        background: Star2,
        title: 'Star Wars: Episode II – Attack of the Clones',
        text: 'Star Wars: Episode II – Attack of the Clones is a 2002 American epic space opera film directed by George Lucas and written by Lucas and Jonathan Hales. It is the second installment of the Star Wars prequel trilogy, and stars Ewan McGregor, Natalie Portman, Hayden Christensen, Ian McDiarmid, Samuel L. Jackson, Christopher Lee, Anthony Daniels, Kenny Baker and Frank Oz.'
      },
      {
        background: Star3,
        title: 'Star Wars: Episode III – Revenge of the Sith',
        text: 'Star Wars: Episode III – Revenge of the Sith is a 2005 American epic space opera film written and directed by George Lucas. It is the sixth entry in the Star Wars film series and stars Hayden Christensen, Ewan McGregor, Natalie Portman, Ian McDiarmid, Samuel L. Jackson, Christopher Lee, Anthony Daniels, Kenny Baker, and Frank Oz. A sequel to The Phantom Menace (1999) and Attack of the Clones (2002), it is the third and final installment in the Star Wars prequel trilogy.'
      }
    ]
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Header/>
            <Switch>
              <Route
                path="/"
                render={(props) => <Home filmCards={this.state.filmCards} {...props}/>}
                exact
              />
              <Route path="/aboutus" component={AboutUs} exact/>
              <Route path="/contacts" component={Contacts} exact/>
            </Switch>
            <Footer/>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

import React from 'react';
import StarWars from '../../assets/img/star4.jpg';

import './AboutUs.css';
import Container from "../Container/Container";

const AboutUs = () => {
  return (
    <div style={{backgroundImage: `url(${StarWars})`}} className="AboutUs">
      <Container>
        <h2>About Us</h2>
        <p><b>Created by:</b> George Lucas</p>
        <p><b>Original work:</b> Star Wars (1977)</p>
        <p><b>Owner:</b>	Lucasfilm (The Walt Disney Company)</p>
        <p><b>Book(s):</b>	List of non-fiction books</p>
        <p><b>Novel(s):</b> List of novels</p>
        <p><b>Comics:</b> List of comics</p>
        <p><b>Film(s):</b> Skywalker saga
          (9 films; 1977–2019)
          The Clone Wars
          (1 pilot film; 2008)
          Anthology
          (2 films; 2016–2018)
          Other
        </p>
        <p><b>Television series:</b> The Mandalorian (2019)</p>
        <p><b>Role-playing:</b> List of RPGs</p>
      </Container>
    </div>
  );
};

export default AboutUs;

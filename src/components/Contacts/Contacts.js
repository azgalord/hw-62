import React from 'react';
import Container from "../Container/Container";
import Map from '../../assets/img/map.png';

import './Contacts.css';

const Contacts = props => {
  return (
    <div className="Contacts">
      <Container>
        <h2>Contacts</h2>
        <p>Tel: +996 559 77 32 83</p>
        <p>Email: lucasfilms@starwars.com</p>
        <p>Address: somewere in USA</p>
        <p>Some text: here</p>
        <img src={Map} alt=""/>
      </Container>
    </div>
  );
};

export default Contacts;

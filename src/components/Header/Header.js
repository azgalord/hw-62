import React from 'react';
import Container from "../Container/Container";
import MainNav from "./MainNav/MainNav";
import Star from '../../assets/img/star.png';
import { NavLink } from 'react-router-dom';

import './Header.css';

const Header = props => {
  return (
      <header className="Header">
        <Container>
          <div style={{backgroundImage: `url(${Star})`}} className="HeaderLogo">
            <NavLink to="/"/>
          </div>
          <MainNav/>
        </Container>
      </header>
  );
};

export default Header;

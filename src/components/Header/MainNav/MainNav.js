import React from 'react';
import { NavLink } from 'react-router-dom';

import './MainNav.css';

const MainNav = props => {
  return (
    <nav className="MainNav">
      <ul>
        <li>
          <NavLink exact activeClassName="Active" to="/">Home</NavLink>
        </li>
        <li>
          <NavLink exact activeClassName="Active" to="/aboutus">About Us</NavLink>
        </li>
        <li>
          <NavLink exact activeClassName="Active" to="/contacts">Contacts</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default MainNav;

import React from 'react';

import './Footer.css';

const Footer = props => {
  return (
    <footer className="Footer">
      <span>Created with love</span>
    </footer>
  );
};

export default Footer;

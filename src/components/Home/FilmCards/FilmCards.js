import React from 'react';

import './FilmCards.css';

const FilmCards = props => {
  return (
    <div className="FilmCards">
      {props.children}
    </div>
  );
};

export default FilmCards;

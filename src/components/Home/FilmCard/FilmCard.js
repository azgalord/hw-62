import React from 'react';

import './FilmCard.css';

const FilmCard = props => {
  return (
    <div className="FilmCard">
      <div
        style={{backgroundImage: `url(${props.background})`}}
        className="FilmCardImg"
      />
      <h2>{props.title}</h2>
      <p>{props.text}</p>
    </div>
  );
};

export default FilmCard;

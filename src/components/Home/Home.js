import React from 'react';
import Container from "../Container/Container";
import FilmCards from "./FilmCards/FilmCards";
import FilmCard from "./FilmCard/FilmCard";

import './Home.css';
import TextInfo from "./TextInfo/TextInfo";

const Home = props => {
  return (
    <div className="Home">
      <Container>
        <FilmCards>
          {props.filmCards.map(filmCard => (
            <FilmCard
              background={filmCard.background}
              title={filmCard.title}
              text={filmCard.text}
            />
          ))}
        </FilmCards>
        <TextInfo/>
      </Container>
    </div>
  );
};

export default Home;
